﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace FirebaseLogin
{
    public partial class MainPage : ContentPage
    {
        userRepository userRepo = new userRepository();
        public MainPage()
        {
            InitializeComponent();
        }

        private async void Sign_In_Clicked(object sender, EventArgs e)
        {
            try
            {
                string email = Email_Controller.Text;
                string password = Password_Controller.Text;

                string token = await userRepo.SignIn(email, password);

                if (!string.IsNullOrEmpty(token))
                {
                    await Navigation.PushAsync(new homePage());
                }
                else
                {
                    await DisplayAlert("Warning", "Sign In failed", "Ouch");
                }
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("EMAIL_NOT_FOUND"))
                {
                    await DisplayAlert("Alert", "Email not found", "Oops");
                }
                else if (ex.Message.Contains("INVALID_PASSWORD"))
                {
                    await DisplayAlert("Alert", "Wrong Password", "Ouch");
                }
                else
                {
                    await DisplayAlert("Alert", ex.Message, "Ok");
                }
            }
          
        }
        private void Sign_Up_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SignUpPage());

        }

        private void SignIn_forgot_password(object sender, EventArgs e)
        {
            Navigation.PushAsync(new forgotPassword());
        }
    }
}
