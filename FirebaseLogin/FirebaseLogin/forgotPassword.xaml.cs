﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirebaseLogin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class forgotPassword : ContentPage
    {
        userRepository userRepo = new userRepository();
        public forgotPassword()
        {
            InitializeComponent();
        }

        private void rememberPassword(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

        private async void sendLinkReset(object sender, EventArgs e)
        {
            string email = ResetEmail.Text;
            if(string.IsNullOrEmpty(email))
            {
                await DisplayAlert("Warning", "Please enter your email address", "Ouch");
                return;
            }
            bool sentMail = await userRepo.sendResetLink(email);
            if (sentMail)
            {
                await DisplayAlert("Information", "Your Reset mail has been sent succesfully", "OK");
            }
            else
            {
                await DisplayAlert("Information", "Your Reset mail has NOT been sent", "Try Again");

            }
        }
    }
}