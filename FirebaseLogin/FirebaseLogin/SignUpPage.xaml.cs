﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirebaseLogin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUpPage : ContentPage
    {
        userRepository userRepo  = new userRepository();
        public SignUpPage()
        {
            InitializeComponent();
        }
        private async void Register_Clicked(object sender, EventArgs e)
        {
            try
            {
                string name = Name_Controller_Register.Text;
                string email = Email_Controller_Register.Text;
                string password = Password_controller_Register.Text;
                string ConPassword = Confirm_Password_controller_Register.Text;

                if (String.IsNullOrEmpty(name))
                {
                    await DisplayAlert("Warning", "Please enter name", "OK");
                    return;
                }
                if (String.IsNullOrEmpty(email))
                {
                    await DisplayAlert("Warning", "Please enter email", "OK");
                    return;
                }
                if (String.IsNullOrEmpty(password))
                {
                    await DisplayAlert("Warning", "Please enter password", "OK");
                    return;
                }
                if (String.IsNullOrEmpty(ConPassword))
                {
                    await DisplayAlert("Warning", "Please enter Confirmation password", "OK");
                    return;
                }
                if (password != ConPassword)
                {
                    await DisplayAlert("Warning", "Password dont match", "OK");
                }
                var isDone = await userRepo.Register(email, name, password);

                if (isDone)
                {
                    await DisplayAlert("Success", "Sign Up sucessful", "YAY");
                    await Navigation.PushAsync(new homePage());
                }
                else
                {
                    await DisplayAlert("Failed", "Sign Up failed", "Nah");

                }
            }
            
            catch(Exception ex)
            {
                if (ex.Message.Contains("EMAIL_EXISTS"))
                {
                    await DisplayAlert("Warning", "Email Already Exists","Oh !!");
                }
                else
                {
                    await DisplayAlert("Error", ex.Message, "Oops");
                }
            }
        }
        private void Cancel_clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}