﻿using Firebase.Auth;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace FirebaseLogin
{
    public  class userRepository
    {
        string api_key = "<your Web Api>";
        FirebaseAuthProvider authProvider;

        public userRepository()
        {
            authProvider = new FirebaseAuthProvider(new FirebaseConfig(api_key));
        }

        public async Task<bool> Register(string email, string name, string password)
        {
           var token = await authProvider.CreateUserWithEmailAndPasswordAsync(email, password, name);
            if(!string.IsNullOrEmpty( token.FirebaseToken))
            {
                return true;
            }
            return false;
        }

        public async Task<string> SignIn (string email,string password)
        {
            var token = await authProvider.SignInWithEmailAndPasswordAsync(email,password);
            if(!string.IsNullOrEmpty(token.FirebaseToken))
            {
                return token.FirebaseToken;
            }
            return "";
        }

        public async Task<bool> sendResetLink(string email)
        {
            await authProvider.SendPasswordResetEmailAsync(email);
            return true;
        }
        


   /*     public bool SignOut()
        {
            try
            {
                authProvider.
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }*/
    }
}
