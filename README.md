# FIREBASE Authentication

## Simplified Firebase Authentication in Xamarin Forms

## Results

<p>
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-firebase-authentication/-/raw/master/img1.png" width="200" height="400">
&nbsp;
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-firebase-authentication/-/raw/master/img2.png" width="200" height="400">
&nbsp;
<img src="https://gitlab.com/Vishwa-Karthik/xamarinbasics-firebase-authentication/-/raw/master/img3.png" width="200" height="400">
</p>
